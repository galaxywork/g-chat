<?php
// demo from: https://segmentfault.com/a/1190000008908533

$admins = [];
$clients = [];
//创建websocket服务器对象，监听0.0.0.0:9502端口
$ws = new swoole_websocket_server("0.0.0.0", 9502);

//监听WebSocket连接打开事件
$ws->on('open', function ($ws, $request) {
    //get admin id and client id by request->get
    var_dump($request->fd, $request->get, $request->server);
    $ws->push($request->fd, respond('hello welcome'));
});

//监听WebSocket消息事件
$ws->on('message', function ($ws, $frame) {
    echo "Message: {$frame->data}\n";
    $ws->push($frame->fd, respond('hello, i am amdin1'));
});

//监听WebSocket连接关闭事件
$ws->on('close', function ($ws, $fd) {
    echo "client-{$fd} is closed\n";
});

function respond($message)
{
    return json_encode([
        'admin_users' => [
            ['id' => 1, 'name' => 'admin1']
        ],
        'message'     => $message
    ]);
}

$ws->start();
